﻿namespace LAB_11
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_temp_water = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label_volog = new System.Windows.Forms.Label();
            this.label_w_sp = new System.Windows.Forms.Label();
            this.label_prea = new System.Windows.Forms.Label();
            this.label_temp = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_temp_water
            // 
            this.label_temp_water.AutoSize = true;
            this.label_temp_water.Location = new System.Drawing.Point(130, 131);
            this.label_temp_water.Name = "label_temp_water";
            this.label_temp_water.Size = new System.Drawing.Size(0, 13);
            this.label_temp_water.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(54, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Темп води";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Вологість";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Тиск";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Шв_вітру";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Температура";
            // 
            // label_volog
            // 
            this.label_volog.AutoSize = true;
            this.label_volog.Location = new System.Drawing.Point(130, 106);
            this.label_volog.Name = "label_volog";
            this.label_volog.Size = new System.Drawing.Size(0, 13);
            this.label_volog.TabIndex = 16;
            // 
            // label_w_sp
            // 
            this.label_w_sp.AutoSize = true;
            this.label_w_sp.Location = new System.Drawing.Point(133, 84);
            this.label_w_sp.Name = "label_w_sp";
            this.label_w_sp.Size = new System.Drawing.Size(0, 13);
            this.label_w_sp.TabIndex = 15;
            // 
            // label_prea
            // 
            this.label_prea.AutoSize = true;
            this.label_prea.Location = new System.Drawing.Point(133, 61);
            this.label_prea.Name = "label_prea";
            this.label_prea.Size = new System.Drawing.Size(0, 13);
            this.label_prea.TabIndex = 14;
            // 
            // label_temp
            // 
            this.label_temp.AutoSize = true;
            this.label_temp.Location = new System.Drawing.Point(130, 38);
            this.label_temp.Name = "label_temp";
            this.label_temp.Size = new System.Drawing.Size(0, 13);
            this.label_temp.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(53, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Погода";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 159);
            this.Controls.Add(this.label_temp_water);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_volog);
            this.Controls.Add(this.label_w_sp);
            this.Controls.Add(this.label_prea);
            this.Controls.Add(this.label_temp);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_temp_water;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_volog;
        private System.Windows.Forms.Label label_w_sp;
        private System.Windows.Forms.Label label_prea;
        private System.Windows.Forms.Label label_temp;
        private System.Windows.Forms.Button button1;
    }
}

