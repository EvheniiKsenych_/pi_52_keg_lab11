﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAB_11
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try {
                WebClient client = new WebClient();
                string site = client.DownloadString("https://www.gismeteo.ua/weather-zhytomyr-4943/");
                Regex temperature = new Regex(@"<dd class='value m_temp c'>(&minus;)?((?:-)?\d+)");
                Regex vindSp = new Regex(@"<dd class='value m_wind ms' style='display:inline'>(\d+)");
                Regex preas = new Regex(@"<dd class='value m_press torr'>(\d+)");
                Regex vilig = new Regex(@"<div class=.+ title=.+>(\d+)");
                Regex tw = new Regex(@"<div class=.wicon water. title=.+>\n\t\t<dd class=.value m_temp c[""]>((?:\+|-)?\d+)");

                Match tmp = temperature.Match(site);
                if(tmp.Groups[1].Value == "&minus;") label_temp.Text= "-" + tmp.Groups[2].Value; 
                else  label_temp.Text = tmp.Groups[2].Value;

                tmp = vindSp.Match(site);
                label_w_sp.Text = tmp.Groups[1].Value + "м\\с";

                tmp = preas.Match(site);
                label_prea.Text = tmp.Groups[1].Value;

                tmp = vilig.Match(site);
                label_volog.Text = tmp.Groups[1].Value;

                tmp = tw.Match(site);
                label_temp_water.Text = tmp.Groups[1].Value;
            }
            catch(Exception)
            {
                MessageBox.Show("ERROR");
            }
        }
    }
}
