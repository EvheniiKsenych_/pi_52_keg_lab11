﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zavd_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Regex t1 = new Regex(@"[_A-Za-z0-9]{8,}");
                Regex t2 = new Regex(@"[A-Z]");
                Regex t3 = new Regex(@"[a-z]");
                Regex t4 = new Regex(@"\d");
                Regex t5 = new Regex(@"[^_A-Za-z0-9]");
                string ini = textBox_in.Text;
                Match tm1 = t1.Match(ini);
                Match tm2 = t2.Match(ini);
                Match tm3 = t3.Match(ini);
                Match tm4 = t4.Match(ini);
                Match tm5 = t5.Match(ini);
                if (tm1.Success && tm2.Success && tm3.Success && tm4.Success && !tm5.Success) label_out_pass.Text = "correct";
                else label_out_pass.Text = "uncorrect";
            }
            catch (Exception)
            {

            }
        }
    }
}
