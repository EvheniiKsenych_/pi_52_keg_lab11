﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace adr
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Regex t1 = new Regex(@"\b(((http|https):\/\/)?([^\W][a-zA-Z\d\.-]+\.)+([/A-Za-z%\d_\.?=&#]+[^\W-])(:\d+)?)\b");
            Regex t2 = new Regex(@"-\.");
            string ini = textBox_in.Text;
            Match tm1 = t1.Match(ini);
            Match tm2 = t2.Match(tm1.Groups[0].Value);
            if (tm1.Success && !tm2.Success)
            {
                textBox1.Text = tm1.Groups[0].Value;
            }

        }
    }
}
