﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zavd_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Regex temperature = new Regex(@"<table(?:.*?:?)>((?:.|\n)*?)<\/table>");
                string ini = textBox_in.Text;
                Match tmp = temperature.Match(ini);
                textBox_out.Text = tmp.Groups[1].Value;
            }
            catch(Exception)
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Regex temperature = new Regex(@"<td(?:.*?:?)>((?:.|\n)*?)<\/td>");
            string ini = textBox_in.Text;
            Match tmp = temperature.Match(ini);
            textBox_out.Text = tmp.Groups[1].Value;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                Regex temperature = new Regex(@"(-?\b(?:(?:[1-9][0-9]*(?:(?:(?:\.|,)\d+)?)|0))\b)");
                string ini = textBox_in.Text;
                Match tmp = temperature.Match(ini);
                textBox_out.Text = tmp.Groups[1].Value;
            }
            catch (Exception)
            {

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                Regex temperature = new Regex(@"((?:(?:[0-9a-zA-Z.\-_]){1,}[^.@\-_])@(?:(?:[\d]|[a-zA-Z]){1,})(?:\.(?:[\d]|[a-zA-Z]){1,})+)");
                string ini = textBox_in.Text;
                Match tmp = temperature.Match(ini);
                textBox_out.Text = tmp.Groups[1].Value;
            }
            catch (Exception)
            {

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                Regex temperature = new Regex(@"<img src=[""](.+)[""]\/>");
                string ini = textBox_in.Text;
                Match tmp = temperature.Match(ini);
                textBox_out.Text = tmp.Groups[1].Value;
            }
            catch (Exception)
            {

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                Regex temperature = new Regex(@"(http:\/\/(?:(?:[\w]+)\.?)+)");
                string ini = textBox_in.Text;
                Match tmp = temperature.Match(ini);
                textBox_out.Text = "<a>"+tmp.Groups[1].Value+"</a>";
            }
            catch (Exception)
            {

            }
        }
    }
}
